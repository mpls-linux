/*
 * sysctl_net_mpls.c: sysctl interface to net MPLS subsystem.
 */

#include <linux/mm.h>
#include <linux/sysctl.h>
#include <net/mpls.h>

static ctl_table mpls_table[] = {
	{
		.ctl_name	= NET_MPLS_DEBUG,
		.procname	= "debug",
		.data		= &sysctl_mpls_debug,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= &proc_dointvec
	},
	{
		.ctl_name	= NET_MPLS_DEFAULT_TTL,
		.procname	= "default_ttl",
		.data		= &sysctl_mpls_default_ttl,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= &proc_dointvec
	},
	{ .ctl_name = 0 }
};

static struct ctl_path mpls_path[] = {
	{ .procname = "net", .ctl_name = CTL_NET, },
	{ .procname = "mpls", .ctl_name = NET_MPLS, },
	{ }
};

static struct ctl_table_header *mpls_table_header;

int __init mpls_sysctl_init(void)
{
	mpls_table_header = register_sysctl_paths(mpls_path, mpls_table);
	if (!mpls_table_header)
		return -ENOMEM;
	return 0;
}

void mpls_sysctl_exit(void)
{
	unregister_sysctl_table(mpls_table_header);
}
